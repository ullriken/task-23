SELECT 
	countrylanguage.language, country.name
FROM 
	countrylanguage INNER JOIN country
ON 
	countrylanguage.CountryCode = country.Code
GROUP BY 
	COUNTRY.NAME
ORDER BY 
	COUNTRY.NAME