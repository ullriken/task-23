SELECT CITY.NAME, CITY.POPULATION, COUNTRY.NAME
FROM CITY INNER JOIN COUNTRY
ON CITY.COUNTRYCODE = COUNTRY.CODE
GROUP BY COUNTRY.NAME
ORDER BY COUNTRY.NAME