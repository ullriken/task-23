Using the world Sample Data from MySQL Workbench

Select all the countries where the average population is more than 1 000 000 per region
countryAvgPopulation.sql

Select all the cities with the City Population and the country name where the city is situated ordered by the name of the country
cityPopAndCountryName.sql

Select all the languages per country, All the languages should be comma separated in 1 column 
and country names should only appear once (No duplicate country names)
languagesPerCountry.sql